# Decoration interieur


## Le problème :

Souvent les familles qui veulent décorer leur maison trouvent une difficulté pour choisir les meubles. Comme il est très difficile de visualiser à quoi ressembleront les meubles dans leurs domiciles. 


## La population cible :

Propriétaires de maisons


## Le projet :

Ce projet, en utilisant la réalité augmentée (AR), va permettre aux gens de chercher dans un catalogue de fourniture des grandes surfaces (Kitea , IKEA ...) et de placer ces meubles dans leurs domiciles en 3D ainsi que la possibilité de les acheter en-ligne. 


## Situation :

Nos mamans, les nouveaux couples mariés et même un nouvel employé cherchent à décorer leur domicile par des nouveaux meubles. Alors ils sont obligés de prendre les mesures et les couleurs de chaque chambre de la maison et d’aller vers un magasin (ou parfois des magasins) pour chercher des meubles qui conviennent aux mesures et couleurs prises. En plus de ça, si ces meubles ne sont pas convenables ils sont forcés de les rendre au magasin pour les changer. Cela demande un effort physique, moral et parfois une perte de temps et d'argent à cause des mauvais choix.


## Les objectifs :

Le projet a pour objectifs : 
Permettre aux utilisateurs de visualiser la décoration intérieure de leur maison virtuellement sans être obligé à se déplacer aux grandes surfaces afin de minimiser les problèmes de transport.
Faciliter l'opération d'achat et la digitaliser (achat en ligne) afin de minimiser le contact entre les gens surtout pendant cette pandémie qu'a connu le monde ces derniers mois.
Permettre aux gens de bien personnaliser leurs domiciles en pouvant voir la totalité du catalogue des meubles au lieu de voir juste ce qui est en stock.

## Les fonctionnalités :

Toute personne qui désire décorer sa maison peut :

- Accéder à des catalogues en ligne et parcourir les articles présentés (des tables, chaises, canapés …) et permettre aux utilisateurs de voir le prix et le rating de chaque article ainsi que le feedback des autres utilisateurs.

- Choisir un article dans le catalogue et le placer en réalité augmentée en temps réel dans un espace de domicile après avoir scanné et détecter la géométrie de la chambre en utilisant le view-finder de l’application. 

- Prendre des photos ou des vidéos des meubles placés et visualisés à travers le view-finder et les partager dans les réseaux sociaux  (Facebook, WhatsApp, Twitter ...) en appuyant sur un bouton de “partager”.

- Effectuer un achat en ligne des articles présentés, un récapitulatif sera présenté dans l’application et un éventuel e-mail est envoyé contenant les articles commandés et le prix total.

- Faire le suivi des commandes à travers un code de suivi mentionné dans le récapitulatif et dans le volet “historique des commandes” dans l’application.



